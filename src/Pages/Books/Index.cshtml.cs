using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using program.Data;
using program.Models;

namespace program.Pages.Books
{
    public class IndexModel : PageModel
    {
        private readonly program.Data.programContext _context;

        public IndexModel(program.Data.programContext context)
        {
            _context = context;
        }

        public IList<Book> Book { get;set; }
        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }

        public async Task OnGetAsync()
        {
            var books = from m in _context.Book
                where m.isReviewed
                select m;

            if(!string.IsNullOrEmpty(SearchString)) {
                books = books.Where(s => s.Title.Contains(SearchString));
            }
            Book = await books.ToListAsync();
        }
    }
}
